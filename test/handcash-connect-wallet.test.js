const { HandcashConnectWallet } = require('../src/wallet/handcash-connect-wallet')
const { def, get } = require('bdd-lazy-var/getter')
const { Address, Tx, Bn } = require('bsv')
const { MockRequester } = require('../src/http/mock-requester')

describe(HandcashConnectWallet, () => {
  def('responseTx', () => {
    const tx = new Tx()
    tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
    tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
    tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
    tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
    tx.addTxOut(new Bn().fromNumber(get.aSatoshisAmount), get.anAddress.toTxOutScript())
    return tx
  })

  def('hcConnect', () => ({
    wallet: {
      pay: jest.fn((props) => {
        const {
          description,
          appAction,
          payments
        } = props

        expect(description).toEqual('fund tx')
        expect(appAction).toEqual('fund-tx')
        expect(payments).toEqual([{
          destination: get.anAddress.toString(),
          currencyCode: 'SAT',
          sendAmount: get.aSatoshisAmount
        }])

        return {
          rawTransactionHex: get.responseTx.toHex(),
          transactionId: get.responseTx.id()
        }
      })
    },
    profile: {
      getCurrentProfile: jest.fn()
    }
  }))

  def('anAddress', () => Address.fromRandom())
  def('aSatoshisAmount', () => 1000)

  def('paymailResponse', () => null)

  def('http', () => new MockRequester(get.paymailResponse))

  def('wallet', () => new HandcashConnectWallet(get.hcConnect, get.http))

  describe('#createUtxo', () => {
    test('calls pay once', async () => {
      await get.wallet.createUtxo(get.aSatoshisAmount, get.anAddress)
      expect(get.hcConnect.wallet.pay).toHaveBeenCalled()
    })

    test('it returns the right data', async () => {
      const response = await get.wallet.createUtxo(get.aSatoshisAmount, get.anAddress)
      expect(response).toEqual({
        txid: get.responseTx.id(),
        vout: get.responseTx.txOuts.length - 1,
        satoshis: get.aSatoshisAmount
      })
    })

    describe('when the output is not present', () => {
      def('responseTx', () => {
        const tx = new Tx()
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        // tx.addTxOut(new Bn().fromNumber(get.aSatoshisAmount), get.anAddress.toTxOutScript())
        return tx
      })

      test('it raises an error', async () => {
        await expect(
          get.wallet.createUtxo(get.aSatoshisAmount, get.anAddress)
        ).rejects.toThrow('output not found')
      })
    })

    describe('when the output is present and has less satoshis', () => {
      def('responseTx', () => {
        const tx = new Tx()
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(get.aSatoshisAmount - 100), get.anAddress.toTxOutScript())
        return tx
      })

      test('it raises an error', async () => {
        await expect(
          get.wallet.createUtxo(get.aSatoshisAmount, get.anAddress)
        ).rejects.toThrow('output not found')
      })
    })

    describe('when the output is present and has more satoshis', () => {
      def('responseTx', () => {
        const tx = new Tx()
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(500), Address.fromRandom().toTxOutScript())
        tx.addTxOut(new Bn().fromNumber(get.aSatoshisAmount + 100), get.anAddress.toTxOutScript())
        return tx
      })

      test('it returns the correct amount of satoshis', async () => {
        const result = await get.wallet.createUtxo(get.aSatoshisAmount, get.anAddress)
        expect(
          result.satoshis
        ).toEqual(get.aSatoshisAmount + 100)
      })
    })

    describe('when handcash trows an error', () => {
      def('anError', () => new Error('some error'))
      def('hcConnect', () => ({
        wallet: {
          pay: jest.fn(() => {
            throw get.anError
          })
        },
        profile: {
          getCurrentProfile: jest.fn()
        }
      }))

      test('it returns the correct amount of satoshis', async () => {
        await expect(
          get.wallet.createUtxo(get.aSatoshisAmount, get.anAddress)
        ).rejects.toBe(get.anError)
      })
    })
  })

  describe('#createPaymentDestination', () => {
    def('paymailResponse', () => ({ output: Address.fromRandom().toTxOutScript().toHex() }))
    def('aPaymail', () => 'finn@adventureti.me')

    def('hcConnect', () => ({
      wallet: {
        pay: jest.fn(() => {
          throw Error('should not be called here')
        })
      },
      profile: {
        getCurrentProfile: jest.fn(() => {
          return { publicProfile: { paymail: get.aPaymail } }
        })
      }
    }))

    it('returns the right script', async () => {
      const response = await get.wallet.createPaymentDestination()
      const responseHex = response.toHex()
      expect(responseHex).toEqual(get.paymailResponse.output)
    })
  })
})
