const { KeyPairProviderRandom } = require('../src/key-pair-provider/key-pair-provider-random')

describe(KeyPairProviderRandom, () => {
  it('returns different key pairs each time', () => {
    const provider = new KeyPairProviderRandom()
    const keyPair1 = provider.getOne()
    const keyPair2 = provider.getOne()
    const keyPair3 = provider.getOne()

    expect(keyPair1.privKey.toWif()).not.toEqual(keyPair2.privKey.toWif())
    expect(keyPair1.privKey.toWif()).not.toEqual(keyPair3.privKey.toWif())
    expect(keyPair2.privKey.toWif()).not.toEqual(keyPair3.privKey.toWif())
  })
})
