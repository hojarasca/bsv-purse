const { Purse } = require('../src/purse')
const { def, get } = require('bdd-lazy-var/getter')
const { Tx, TxBuilder, Bn, Address, KeyPair, Interp, Script } = require('bsv')
const { TestWallet } = require('../src/wallet/test-wallet')
const { DUST_SATOSHIS } = require('../src/constants')
const { KeyPairProviderTest } = require('../src/key-pair-provider/key-pair-provider-test')

describe(Purse, () => {
  def('changeAddress', () => null)

  def('aWallet', () => new TestWallet(get.changeAddress))

  def('keyPairProvider', () => new KeyPairProviderTest(get.intermediaryKeyPair))

  def('purse', () => new Purse(get.aWallet, get.keyPairProvider))

  describe('#fund', () => {
    def('targetAddress', Address.fromRandom())
    def('txToFund', () => {
      const txBuilder = new TxBuilder()
      txBuilder.outputToAddress(new Bn().fromNumber(1000), get.targetAddress)
      txBuilder.buildOutputs()
      return txBuilder.tx
    })

    def('intermediaryKeyPair', () => KeyPair.fromRandom())

    describe('when there is a simple p2pkh output to fund', () => {
      test('returns a valid hex transaction', async () => {
        const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), [])
        expect(fundedTxHex).toMatch(/^[a-fA-F0-9]+$/) // its a hex
        expect(() => Tx.fromHex(fundedTxHex)).not.toThrow()
      })

      test('adds an input', async () => {
        const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), [])
        const fundedTx = Tx.fromHex(fundedTxHex)
        expect(fundedTx.txIns).toHaveLength(1)
      })

      test('preserves the same outputs', async () => {
        const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), [])
        const fundedTx = Tx.fromHex(fundedTxHex)
        const txOutFromFundedTx = fundedTx.txOuts[0]
        expect(fundedTx.txOuts).toHaveLength(1)
        expect(fundedTx.txOuts[0]).toEqual(txOutFromFundedTx)
      })

      test('requests at least the amount of the output', async () => {
        await get.purse.pay(get.txToFund.toHex(), [])
        const { satoshis } = get.aWallet.lastRequest()
        expect(satoshis).toBeGreaterThan(get.txToFund.txOuts[0].valueBn.toNumber())
      })

      test('requests the exact amount needed to fund the tx', async () => {
        await get.purse.pay(get.txToFund.toHex(), [])
        const { satoshis } = get.aWallet.lastRequest()
        expect(satoshis).toEqual(1114)
      })

      test('creates an input using the provided intermediary address', async () => {
        const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), [])
        const fundedTx = Tx.fromHex(fundedTxHex)
        const txIn = fundedTx.txIns[0]
        const addressFromInput = Address.fromTxInScript(txIn.script).toString()
        const providedAddress = Address.fromPubKey(get.intermediaryKeyPair.pubKey).toString()
        expect(addressFromInput).toEqual(providedAddress)
      })

      test.skip('creates an input that can be verified', async () => {
        const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), [])
        const fundedTx = Tx.fromHex(fundedTxHex)
        const txIn = fundedTx.txIns[0]

        const interp = new Interp()
        const flags =
          Interp.SCRIPT_VERIFY_P2SH | Interp.SCRIPT_VERIFY_STRICTENC |
          Interp.SCRIPT_ENABLE_SIGHASH_FORKID | Interp.SCRIPT_VERIFY_LOW_S | Interp.SCRIPT_VERIFY_NULLFAIL

        const verified = interp.verify(
          txIn.script,
          Address.fromPrivKey(get.intermediaryKeyPair.privKey).toTxOutScript(),
          fundedTx,
          0,
          flags
        )

        expect(verified).toBe(true)
      })
    })

    describe('when there is 2 p2pkh outputs', () => {
      def('targets', () => [
        {
          address: Address.fromRandom(),
          satoshis: 1000
        },
        {
          address: Address.fromRandom(),
          satoshis: 2000
        }
      ])
      def('txToFund', () => {
        const txBuilder = new TxBuilder()
        get.targets.forEach(({ address, satoshis }) => {
          txBuilder.outputToAddress(new Bn().fromNumber(satoshis), address)
        })
        txBuilder.buildOutputs()
        return txBuilder.tx
      })

      test('requests more than the sum of the outputs and the size of the original tx', async () => {
        await get.purse.pay(get.txToFund.toHex(), [])
        const { satoshis } = get.aWallet.lastRequest()
        expect(satoshis).toBeGreaterThan(3000)
      })

      test('request the right amount of satoshis', async () => {
        await get.purse.pay(get.txToFund.toHex(), [])
        const { satoshis } = get.aWallet.lastRequest()
        const totalOutputs = 3000
        const sizeOfNonFundedTx = Buffer.byteLength(get.txToFund.toBuffer()) / 2
        const feesForExtraInput = 92
        expect(satoshis).toEqual(
          totalOutputs +
          sizeOfNonFundedTx +
          feesForExtraInput
        )
      })
    })

    describe('when there is several p2pkh outputss and 2 pre existing inputs', () => {
      def('targets', () => [
        {
          address: Address.fromRandom(),
          satoshis: 1000
        },
        {
          address: Address.fromRandom(),
          satoshis: 2000
        }
      ])

      def('parents', () => [
        {
          script: 'OP_TRUE',
          satoshis: 500
        },
        {
          script: 'OP_TRUE',
          satoshis: 600
        }
      ])

      def('txToFund', () => {
        const tx = new Tx()
        get.targets.forEach(({ address, satoshis }) => {
          tx.addTxOut(new Bn().fromNumber(satoshis), address.toTxOutScript())
        })

        get.parents.forEach((_parent) => {
          tx.addTxIn(
            Buffer.alloc(32).fill(0),
            0,
            Script.fromAsmString('OP_TRUE')
          )
        })

        return tx
      })

      test('fails if the amount of parents is not correct', async () => {
        await expect(() => get.purse.pay(get.txToFund.toHex(), [])).rejects.toEqual(new Error('inputs and parents do not match'))
      })

      test('substract the input amount from the requested satoshis', async () => {
        await get.purse.pay(get.txToFund.toHex(), get.parents)
        const { satoshis } = get.aWallet.lastRequest()
        const totalOutputs = get.targets.reduce((total, target) => total + target.satoshis, 0)
        const totalInputs = get.parents.reduce((total, parent) => total + parent.satoshis, 0)
        const sizeOfNonFundedTx = Buffer.byteLength(get.txToFund.toBuffer()) / 2
        const feesForExtraInput = 92
        expect(satoshis).toEqual(
          totalOutputs +
          sizeOfNonFundedTx +
          feesForExtraInput -
          totalInputs
        )
      })

      test('preserves the inputs', async () => {
        const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), get.parents)
        const fundedTx = Tx.fromHex(fundedTxHex)

        expect(fundedTx.txIns).toHaveLength(3)
        expect(fundedTx.txIns[0]).toEqual(get.txToFund.txIns[0]) // first old input
        expect(fundedTx.txIns[1]).toEqual(get.txToFund.txIns[1]) // second old input
      })

      test('adds an extra input to the right address', async () => {
        const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), get.parents)
        const fundedTx = Tx.fromHex(fundedTxHex)

        const extraInput = fundedTx.txIns[2]

        const extraInputAddressStr = Address.fromTxInScript(extraInput.script).toString()
        expect(extraInputAddressStr).toEqual(Address.fromPubKey(get.intermediaryKeyPair.pubKey).toString())
      })

      describe('when the amount in the inputs is exactly the same as the amount in the outputs', () => {
        def('targets', () => [
          {
            address: Address.fromRandom(),
            satoshis: 1000
          },
          {
            address: Address.fromRandom(),
            satoshis: 1000
          },
          {
            address: Address.fromRandom(),
            satoshis: 1000
          },
          {
            address: Address.fromRandom(),
            satoshis: 1000
          },
          {
            address: Address.fromRandom(),
            satoshis: 1000
          },
          {
            address: Address.fromRandom(),
            satoshis: 1000
          },
          {
            address: Address.fromRandom(),
            satoshis: 2000
          }
        ])

        def('parents', () => {
          return get.targets.map(target => ({
            script: 'OP_TRUE',
            satoshis: target.satoshis
          }))
        })

        test('request only to pay the fee', async () => {
          await get.purse.pay(get.txToFund.toHex(), get.parents)
          const { satoshis } = get.aWallet.lastRequest()
          const feesOfNonFundedTx = Buffer.byteLength(get.txToFund.toBuffer()) / 2
          const feesForExtraInput = 92
          expect(satoshis).toEqual(feesOfNonFundedTx + feesForExtraInput)
        })
      })
    })

    describe('when the difference between the inputs and the bsv needed is less than dust', () => {
      def('amounts', () => [1000, 2000])
      def('targets', () => get.amounts.map(amount => ({
        address: Address.fromRandom(),
        satoshis: amount
      })))

      def('parents', () => {
        const totalOutputs = get.amounts.reduce((total, amount) => total + amount, 0)
        const sizeOfNonFundedTx = Buffer.byteLength(get.txToFund.toBuffer()) / 2
        const feesForExtraInput = 92
        const missingSats = 10

        const satoshis = totalOutputs + sizeOfNonFundedTx + feesForExtraInput - missingSats
        return [{ script: 'OP_TRUE', satoshis }]
      })

      def('txToFund', () => {
        const tx = new Tx()
        get.targets.forEach(({ address, satoshis }) => {
          tx.addTxOut(new Bn().fromNumber(satoshis), address.toTxOutScript())
        })

        tx.addTxIn( // Only one input in this example
          Buffer.alloc(32).fill(0),
          0,
          Script.fromAsmString('OP_TRUE')
        )

        return tx
      })

      test('requests for dust', async () => {
        await get.purse.pay(get.txToFund.toHex(), get.parents)
        const { satoshis } = get.aWallet.lastRequest()
        const dust = DUST_SATOSHIS
        expect(satoshis).toEqual(dust)
      })
    })

    describe('when there is 1 input and several outputs', () => {
      def('amounts', () => [1000, 2000])

      def('targets', () => get.amounts.map(amount => ({
        address: Address.fromRandom(),
        satoshis: amount
      })))

      def('extraSants', () => null)

      def('parents', () => {
        const totalOutputs = get.amounts.reduce((total, amount) => total + amount, 0)
        const sizeOfNonFundedTx = Buffer.byteLength(get.txToFund.toBuffer()) / 2
        const feesForExtraInput = 92
        const extraSats = get.extraSats

        const satoshis = totalOutputs + sizeOfNonFundedTx + feesForExtraInput + extraSats
        return [{ script: 'OP_TRUE', satoshis }]
      })

      def('txToFund', () => {
        const tx = new Tx()
        get.targets.forEach(({ address, satoshis }) => {
          tx.addTxOut(new Bn().fromNumber(satoshis), address.toTxOutScript())
        })

        tx.addTxIn( // Only one input in this example
          Buffer.alloc(32).fill(0),
          0,
          Script.fromAsmString('OP_TRUE')
        )

        return tx
      })

      describe('when the inputs provide more bsv than needed, but the difference is less than dust', () => {
        def('extraSats', () => 10)

        test('returns the same tx', async () => {
          const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), get.parents)
          expect(fundedTxHex).toEqual(get.txToFund.toHex())
        })
      })

      describe('when the inputs provide more bsv than needed, but the difference is more than dust', () => {
        def('extraSats', () => DUST_SATOSHIS + 1000)
        def('changeAddress', () => Address.fromRandom())

        test('does not change the amount of inputs', async () => {
          const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), get.parents)
          const fundedTx = Tx.fromHex(fundedTxHex)
          expect(fundedTx.txIns.length).toEqual(get.txToFund.txIns.length)
        })

        test('adds one output', async () => {
          const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), get.parents)
          const fundedTx = Tx.fromHex(fundedTxHex)
          expect(fundedTx.txOuts.length).toEqual(get.txToFund.txOuts.length + 1)
        })

        test('uses the right address in the extra output', async () => {
          const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), get.parents)
          const fundedTx = Tx.fromHex(fundedTxHex)
          const txOut = fundedTx.txOuts[fundedTx.txOuts.length - 1]
          const changeAddress = Address.fromTxOutScript(txOut.script)
          expect(changeAddress.toString()).toEqual(get.changeAddress.toString())
        })

        test('the extra output has less satoshis than the difference between inputs and outputs', async () => {
          const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), get.parents)
          const fundedTx = Tx.fromHex(fundedTxHex)
          const otherOutputsSats =
            fundedTx.txOuts[0].valueBn.toNumber() +
            fundedTx.txOuts[1].valueBn.toNumber()
          const inputSats = get.parents[0].satoshis
          const extraOutputsSats = fundedTx.txOuts[fundedTx.txOuts.length - 1].valueBn.toNumber()

          expect(extraOutputsSats).toBeLessThan(inputSats - otherOutputsSats)
        })

        test('the tx keeps paying right fees', async () => {
          const fundedTxHex = await get.purse.pay(get.txToFund.toHex(), get.parents)
          const fundedTx = Tx.fromHex(fundedTxHex)
          const outputSats = fundedTx.txOuts.map(txOut => txOut.valueBn.toNumber()).reduce((total, num) => total + num, 0)
          const inputSats = get.parents[0].satoshis
          expect(inputSats).toBeGreaterThan(outputSats)
          expect(inputSats - outputSats).toBeGreaterThan(Buffer.byteLength(fundedTx.toBuffer()) * 0.5)
        })
      })
    })
  })
})
