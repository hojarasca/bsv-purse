const {
  Purse,
  HandcashConnectWallet,
  KeyPairProviderRandom
} = require('.')
const { HandCashConnect } = require('@handcash/handcash-connect')
const Run = require('run-sdk')

const handCashConnect = new HandCashConnect(process.env.HC_APP_ID)
const handCashAccount = handCashConnect.getAccountFromAuthToken(process.env.HC_USER_TOKEN)

const wallet = new HandcashConnectWallet(handCashAccount)

const purse = new Purse(wallet, new KeyPairProviderRandom())

const run = new Run({ purse, owner: process.env.RUN_OWNER_WIF }) // please use a wif under your control

class SimpleStore extends Run.Jig {
  set (value) {
    this.value = value
  }
}

const store = new SimpleStore('hello run!')

run.sync().then(() => {
  console.log(store.location)
})
