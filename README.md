# Simple RUN purse implementation.

This is a really simple (Run)[https://run.network] purse. The objective of this
library is fund jig transaction using different bsv wallets. At the moment the
only one supported is (Handcash)[https://handcash.io] trough (handcash
connect)[https://handcash.dev]. But in the future we plan to support at least
Money Button and DotWallet.

## How to install

with  yarn

`yarn add bsv-purse`

with npm

`npm install bsv-purse --save`

## How to use

The file `simple-example.js` contains some details on how to use it.

## How it works

Run's purse basically funds arbitrary transaction. The idea of the purse is that
it receives a hex tx with less balance in the inputs than in the outputs and
returns a new version of that tx, that includes new inputs to pay the fees and
fund the outputs.

The intuitive way to do that us just own some utxos and grab them into the tx.
The issue is that most of the wallets on BSV does not provide API's to easily do
someting like that. The workaround used in this library is perform an
intermediary transaction.

It basically calculates how many satoshis the transaction needs to be funded.
Then it creates a tx using some wallet api (handcash in this case) creating a
utxo that can be unlocked with a temporal private key. That utxo is finally used
to fund the transaction.

This method has a big upside, wich is the simplicity. But it has also 2 big
downsides, it's not super fast and not super cheap in fees, because you are
basically making 2 txs instead of one. Still, non of this is so terrible,
because BSV has fast and cheap transactions.

## Is it production ready?

I don't think so. It's been only tested in mainnet with success, but not in real
world production apps. But I'm working to make it more stable. The idea of that
eventually it allows devs to customize all the behavior, so it can be used in
real production apps.

## Next steps

### Getting started with handcash

If you are not familiar with (handcash connect)[https://handcash.dev] you should
go and read (their docs)[https://docs.handcash.dev/]. They have good examples.
You are going to need to create a set of app keys. Handcash connect is a server
side tool, so you are going to need to setup some simple http server to be able
to take 100% adventage of this.

### Some advance ussage

Part of the idea of this library is that the behavior can be tweeked injecting dependencies.
Right now there are 3 interfaces that define behavior. Non of them are final or set in stone:

``` js
class Wallet {
  createUtxo (_satoshis, _address) {} // Returns a funded utxo

  createPaymentDestination () {} // Returns an output
}

class KeyPairProvider {
  getOne () {} // Returns a keypair
}
```
