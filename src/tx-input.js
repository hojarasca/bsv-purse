const { Tx, Address, Script, Sig, Bn } = require('bsv')

class TxInput {
  constructor (txid, vout, satoshis, keyPair) {
    this.utxo = {
      txid: txid,
      vout: vout,
      satoshis: satoshis
    }
    this.keyPair = keyPair
  }

  addToTx (tx) {
    tx.addTxIn(
      Buffer.from(this.utxo.txid, 'hex').reverse(),
      this.utxo.vout,
      new Script([])
    )
    this.vin = tx.txIns.length - 1
  }

  signTx (tx) {
    const sig = tx.sign(
      this.keyPair,
      Sig.SIGHASH_ALL | Sig.SIGHASH_FORKID,
      this.vin,
      Address.fromPubKey(this.keyPair.pubKey).toTxOutScript(),
      new Bn().fromNumber(this.utxo.satoshis),
      Tx.SCRIPT_ENABLE_SIGHASH_FORKID
    )
    const txIn = tx.txIns[this.vin]
    const script = new Script([])
    script.writeBuffer(sig.toTxFormat())
    script.writeBuffer(this.keyPair.pubKey.toBuffer())
    txIn.setScript(script)
  }
}

module.exports = { TxInput }
