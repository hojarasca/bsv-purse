const { Address } = require('bsv')
const { DUST_SATOSHIS, FEE_PER_BYTE, SIZE_EXTRA_INPUT } = require('./constants')
const { Transaction } = require('./transaction')
const { TxInput } = require('./tx-input')

class Purse {
  constructor (aWallet, keyPairProvider) {
    this.wallet = aWallet
    this.keyPairProvider = keyPairProvider
  }

  /**
   *
   * @param {string} rawTx - hex string raw tx
   * @returns hex string for funded raw tx
   */
  async pay (rawTx, parents) {
    const tx = new Transaction(rawTx, parents)

    const intermediaryKeyPair = this.keyPairProvider.getOne()

    const missingSatoshis = this._estimateMissingSatoshis(tx, parents)

    if (missingSatoshis > 0) {
      const amountToRequest = Math.max(missingSatoshis, DUST_SATOSHIS)
      const address = Address.fromPrivKey(intermediaryKeyPair.privKey)

      const { txid, vout, satoshis } = await this.wallet.createUtxo(amountToRequest, address)
      const input = new TxInput(txid, vout, satoshis, intermediaryKeyPair)
      tx.addInput(input)
      tx.sign()
      return tx.toHex()
    } else if (missingSatoshis < -DUST_SATOSHIS) {
      tx.addChangeOutput(this.wallet.createPaymentDestination())
      return tx.toHex()
    } else {
      return rawTx
    }
  }

  _estimateMissingSatoshis (tx) {
    const estimatedAmount = tx.outputSatohis() +
      Math.ceil(tx.originalTxSizeBytes() * FEE_PER_BYTE) -
      Math.ceil(tx.inputSatoshis()) +
      Math.ceil(SIZE_EXTRA_INPUT * FEE_PER_BYTE) // Size of extra input
    return estimatedAmount
  }
}

module.exports = { Purse }
