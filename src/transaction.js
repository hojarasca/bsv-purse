const { Tx, Bn } = require('bsv')
const { DUST_SATOSHIS } = require('./constants')

class Transaction {
  constructor (txHex, parents) {
    this.tx = Tx.fromHex(txHex)
    if (this.tx.txIns.length !== parents.length) {
      throw new Error('inputs and parents do not match')
    }
    this.parents = parents
    this.extraInputs = []
  }

  originalTxSizeBytes () {
    return Buffer.byteLength(this.tx.toBuffer())
  }

  inputCount () {
    return this.tx.txIns.length
  }

  inputSatoshis () {
    return this.parents.reduce((total, parent) => total + parent.satoshis, 0)
  }

  outputSatohis () {
    return this.tx.txOuts
      .map(txOut => txOut.valueBn.toNumber())
      .reduce((total, satoshis) => total + satoshis, 0)
  }

  addInput (input) {
    input.addToTx(this.tx)
    this.extraInputs.push(input)
    return this
  }

  addChangeOutput (changeScript, missingSatoshis) {
    missingSatoshis = Math.max(missingSatoshis, DUST_SATOSHIS)
    this.tx.addTxOut(new Bn().fromNumber(missingSatoshis), changeScript)
    return this
  }

  toHex () {
    return this.tx.toHex()
  }

  sign () {
    this.extraInputs.forEach((input) => {
      input.signTx(this.tx)
    })
  }
}

module.exports = { Transaction }
