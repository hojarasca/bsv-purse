const { Requester } = require('./requester')

class MockRequester extends Requester {
  constructor (nextResponse) {
    super()
    this._nextResponse = nextResponse
    this._lastRequest = null
  }

  async postJson (url, body) {
    this._lastRequest = { url, body }
    return this._nextResponse
  }

  /**
   * Mock methods
   */

  lastRequest () {
    return this._lastRequest
  }
}

module.exports = { MockRequester }
