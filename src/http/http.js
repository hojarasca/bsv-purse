const fetch = require('isomorphic-fetch')

class Http {
  async postJson (url, body) {
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(body)
    })

    return response.json()
  }
}

module.exports = { Http }
