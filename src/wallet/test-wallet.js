const { Wallet } = require('./wallet')

class TestWallet extends Wallet {
  constructor (changeAddress) {
    super()
    this._lastRequest = null
    this._changeAddress = changeAddress
  }

  createUtxo (satoshis, address) {
    this._lastRequest = { satoshis, address }
    return {
      txid: Buffer.alloc(32).fill(1),
      vout: 0,
      satoshis
    }
  }

  createPaymentDestination () {
    return this._changeAddress.toTxOutScript()
  }

  /**
   * Mock methods
   */

  lastRequest () {
    if (this._lastRequest === null) {
      throw new Error('no bsv was requested yet')
    }
    return this._lastRequest
  }
}

module.exports = { TestWallet }
