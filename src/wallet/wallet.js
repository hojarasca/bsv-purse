class Wallet {
  createUtxo (_satoshis, _address) {
    throw new Error('should be implemented')
  }

  createPaymentDestination () {
    throw new Error('should be implemented')
  }
}

module.exports = { Wallet }
