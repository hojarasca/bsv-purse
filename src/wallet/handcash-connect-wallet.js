const { Tx, Address, Script } = require('bsv')
const { Http } = require('../http/http')
const { Wallet } = require('./wallet')

class HandcashConnectWallet extends Wallet {
  constructor (hcConnectAccount, http = new Http()) {
    super()
    this.account = hcConnectAccount
    this.http = http
  }

  async createUtxo (satoshis, address) {
    const payment = await this.account.wallet.pay({
      description: 'fund tx',
      appAction: 'fund-tx',
      payments: [
        { destination: address.toString(), currencyCode: 'SAT', sendAmount: satoshis }
      ]
    })

    const tx = Tx.fromHex(payment.rawTransactionHex)

    const vout = tx.txOuts.findIndex(txOut => {
      const rightSatoshis = txOut.valueBn.toNumber() >= satoshis
      const rightAddress = Address.fromTxOutScript(txOut.script).toString() === address.toString()
      return rightSatoshis && rightAddress
    })

    if (vout < 0) {
      throw new Error('output not found')
    }

    return {
      txid: payment.transactionId,
      vout: vout,
      satoshis: tx.txOuts[vout].valueBn.toNumber()
    }
  }

  async createPaymentDestination () {
    const profile = await this.account.profile.getCurrentProfile()
    const { output: outputHex } = await this.http.postJson(
      `https://cloud.handcash.io/api/bsvalias/address/${profile.publicProfile.paymail}`,
      {
        senderHandle: profile.publicProfile.paymail,
        amount: 500,
        senderName: profile.publicProfile.displayName,
        dt: new Date().toISOString(),
        purpose: 'change from fund tx'
      }
    )
    return Script.fromHex(outputHex)
  }
}

module.exports = { HandcashConnectWallet }
