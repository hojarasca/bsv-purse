const { KeyPairProvider } = require('./key-pair-provider')

class KeyPairProviderTest extends KeyPairProvider {
  constructor (aKeyPair) {
    super()
    this.aKeyPair = aKeyPair
  }

  getOne () {
    return this.aKeyPair
  }
}

module.exports = { KeyPairProviderTest }
