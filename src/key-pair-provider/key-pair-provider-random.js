const { KeyPair } = require('bsv')
const { KeyPairProvider } = require('./key-pair-provider')

class KeyPairProviderRandom extends KeyPairProvider {
  getOne () {
    return KeyPair.fromRandom()
  }
}

module.exports = { KeyPairProviderRandom }
