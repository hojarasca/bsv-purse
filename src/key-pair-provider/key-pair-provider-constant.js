const { KeyPairProvider } = require('./key-pair-provider')
const { KeyPair, PrivKey } = require('bsv')

class KeyPairProviderConstant extends KeyPairProvider {
  constructor (wif) {
    super()
    this.keyPair = KeyPair.fromPrivKey(PrivKey.fromWif(wif))
  }

  getOne () {
    return this.keyPair
  }
}

module.exports = { KeyPairProviderConstant }
